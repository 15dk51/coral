import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable()
export class HeroslideService {
  public heroslideLength: any;
  public $heroslideLength: any;
  constructor() { }
  heroslideInit() {
    let i = 0;
    for (i; i < this.heroslideLength; i++) {
      $('.hero-banner .slider-controls .navs').append('<span class="slider-nav"></span>');
    }
    $('.hero-banner .slider-controls .navs span:eq(0)').addClass('active');
    $('.hero-banner .slide-item:eq(0)').addClass('active active-text');
  }
  heroslideNext() {
    let activeSlide = $('.hero-banner .slide-item.active').index();
    $('.hero-banner').removeClass('stop-click');
    $('.hero-banner .slide-item').removeClass('prevactive zoom-in active zoom-out active-text');
    $('.hero-banner .slide-item:eq(' + activeSlide + ')').addClass('prevactive zoom-in').removeClass('active');
    $('.hero-banner .slider-controls .navs span:eq(' + activeSlide + ')').removeClass('active');
    setTimeout(function () {
      $('.hero-banner .slide-item.prevactive').removeClass('prevactive zoom-in active zoom-out active-text');
      $('.hero-banner').addClass('stop-click');
    }, 1100);
    activeSlide++;
    if (activeSlide > this.$heroslideLength) {
      activeSlide = 0;
    }
    $('.hero-banner .slider-controls .navs span:eq(' + activeSlide + ')').addClass('active');
    $('.hero-banner .slide-item:eq(' + activeSlide + ')').addClass('active zoom-out active-text');
  }
  heroslidenextClick() {
    $('.hero-banner .next').click(() => { if ($('.hero-banner').hasClass('stop-click')) {this.heroslideNext(); } } );
  }
  heroslidePrev() {
    let activeSlide = $('.hero-banner .slide-item.active').index();
    $('.hero-banner').removeClass('stop-click');
    $('.hero-banner .slide-item:eq(' + activeSlide + ')').removeClass('active zoom-in').addClass('prevactive zoom-out');
    $('.hero-banner .slider-controls .navs span:eq(' + activeSlide + ')').removeClass('active');
    setTimeout(function () {
      $('.hero-banner .slide-item.prevactive').removeClass('prevactive zoom-in active zoom-out active-text');
      $('.hero-banner').addClass('stop-click');
    }, 1100);
    activeSlide--;
    if (activeSlide < 0) {
      activeSlide = this.$heroslideLength;
    }
    $('.hero-banner .slider-controls .navs span:eq(' + activeSlide + ')').addClass('active');
    $('.hero-banner .slide-item:eq(' + activeSlide + ')').addClass('active zoom-in active-text');
  }
  heroslideprevClick() {
    $('.hero-banner .prev').click(() => { if ($('.hero-banner').hasClass('stop-click')) {this.heroslidePrev(); } } );
  }
  imageResponsive(param) {
    $(param).each(function () {
      let img;
      let defaultWidth, defaultHeight, parentHeight, parentWidth, aspectRatio ;
      img = $(this).children('img');
      defaultWidth = img.prop('naturalWidth');
      defaultHeight = img.prop('naturalHeight');
      parentHeight = $(this).height();
      parentWidth = $(this).width();
      aspectRatio = defaultWidth / defaultHeight;
      img.css({'height': 'auto', 'width': '100%', 'margin-left': '0px'});
      let imgHeight = parentWidth / aspectRatio;
      let imgTop = (imgHeight - parentHeight) / 2;
      img.css({'margin-top': '-' + imgTop + 'px'});
      if (img.height() < parentHeight) {
        img.css({'height': '100%', 'width': 'auto'});
        const right_margin = (img.width() - parentWidth) / 2;
        img.css({'margin-left': '-' + right_margin + 'px', 'margin-top': '0'});
      } else if (img.width() < parentWidth) {
        img.css({'height': 'auto', 'width': '100%', 'margin-left': '0'});
        imgHeight = parentWidth / aspectRatio;
        imgTop = (imgHeight - parentHeight) / 2;
        img.css({'margin-top': '-' + imgTop + 'px'});
      }
    });
  }

  setupheroslide() {
    $('.hero-banner').addClass('on-load-animate');
    $('.hero-banner').addClass('stop-click');
    this.heroslideLength = $('.hero-banner .slide-item').length;
    this.$heroslideLength = (this.heroslideLength - 1);
    this.heroslideInit();
    this.heroslidenextClick();
    this.heroslideprevClick();
  }


}
