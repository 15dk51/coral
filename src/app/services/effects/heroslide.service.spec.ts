import { TestBed, inject } from '@angular/core/testing';

import { HeroslideService } from './heroslide.service';

describe('HeroslideService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroslideService]
    });
  });

  it('should be created', inject([HeroslideService], (service: HeroslideService) => {
    expect(service).toBeTruthy();
  }));
});
