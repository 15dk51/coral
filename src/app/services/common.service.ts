import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class CommonService {
  constructor(private http: HttpClient ) { }

  getContent(endpoint): Observable <any> {

    return this.http.get(endpoint);
  }
}
