import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textSearch',
  pure: false
})
export class TextSearchPipe implements PipeTransform {

  transform(items: any, filterQuery: string): any {
    if (!items) {return []; }
    return items.filter( item => {
      if (item.taxonomy !== undefined) {
        return item.taxonomy.toLowerCase().includes(filterQuery.toLowerCase());
      }
    });
  }

}
