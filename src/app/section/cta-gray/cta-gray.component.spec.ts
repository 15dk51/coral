import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtaGrayComponent } from './cta-gray.component';

describe('CtaGrayComponent', () => {
  let component: CtaGrayComponent;
  let fixture: ComponentFixture<CtaGrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtaGrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtaGrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
