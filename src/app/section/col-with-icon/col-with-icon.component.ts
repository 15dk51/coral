import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-col-with-icon',
  templateUrl: './col-with-icon.component.html',
  styleUrls: ['./col-with-icon.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ColWithIconComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
