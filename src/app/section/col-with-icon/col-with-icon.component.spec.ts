import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColWithIconComponent } from './col-with-icon.component';

describe('ColWithIconComponent', () => {
  let component: ColWithIconComponent;
  let fixture: ComponentFixture<ColWithIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColWithIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColWithIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
