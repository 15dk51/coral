import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColTwoWithVideoComponent } from './col-two-with-video.component';

describe('ColTwoWithVideoComponent', () => {
  let component: ColTwoWithVideoComponent;
  let fixture: ComponentFixture<ColTwoWithVideo>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColTwoWithVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColTwoWithVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
