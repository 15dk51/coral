import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { HeroslideService } from '../../services/effects/heroslide.service';

@Component({
  selector: 'app-hero-slider',
  templateUrl: './hero-slider.component.html',
  styleUrls: ['./hero-slider.component.scss']
})
export class HeroSliderComponent implements OnInit {
    @Input() items: any;


  constructor( public _HeroslideService: HeroslideService) {}

  ngOnInit() {
    this._HeroslideService.setupheroslide();
  }

}
