import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'app-col-image-cta',
  templateUrl: './col-image-cta.component.html',
  styleUrls: ['./col-image-cta.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ColImageCtaComponent implements OnInit {
  @Input() items: any;
  constructor() { }

  ngOnInit() {
  }

}
