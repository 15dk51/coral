import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColImageCtaComponent } from './col-image-cta.component';

describe('ColImageCtaComponent', () => {
  let component: ColImageCtaComponent;
  let fixture: ComponentFixture<ColImageCtaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColImageCtaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColImageCtaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
