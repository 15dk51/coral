import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyWithImageComponent } from './body-with-image.component';

describe('BodyWithImageComponent', () => {
  let component: BodyWithImageComponent;
  let fixture: ComponentFixture<BodyWithImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyWithImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyWithImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
