import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColFourComponent } from './col-four.component';

describe('ColFourComponent', () => {
  let component: ColFourComponent;
  let fixture: ComponentFixture<ColFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
