import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-col-four',
  templateUrl: './col-four.component.html',
  styleUrls: ['./col-four.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ColFourComponent implements OnInit {
  @Input() items: any;
  constructor() { }

  ngOnInit() {
  }

}
