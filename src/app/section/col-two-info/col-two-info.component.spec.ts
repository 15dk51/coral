import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColTwoInfoComponent } from './col-two-info.component';

describe('ColTwoInfoComponent', () => {
  let component: ColTwoInfoComponent;
  let fixture: ComponentFixture<ColTwoInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColTwoInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColTwoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
