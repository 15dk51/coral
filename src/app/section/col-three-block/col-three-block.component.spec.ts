import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColThreeBlockComponent } from './col-three-block.component';

describe('ColThreeBlockComponent', () => {
  let component: ColThreeBlockComponent;
  let fixture: ComponentFixture<ColThreeBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColThreeBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColThreeBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
