import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-level-banner',
  templateUrl: './second-level-banner.component.html',
  styleUrls: ['./second-level-banner.component.scss']
})
export class SecondLevelBannerComponent implements OnInit {
  @Input() item: any;
  constructor() { }

  ngOnInit() {
  }

}
