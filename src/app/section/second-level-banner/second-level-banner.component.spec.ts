import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondLevelBannerComponent } from './second-level-banner.component';

describe('SecondLevelBannerComponent', () => {
  let component: SecondLevelBannerComponent;
  let fixture: ComponentFixture<SecondLevelBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondLevelBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondLevelBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
