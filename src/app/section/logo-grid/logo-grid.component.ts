import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-logo-grid',
  templateUrl: './logo-grid.component.html',
  styleUrls: ['./logo-grid.component.scss']
})
export class LogoGridComponent implements OnInit {
  @Input() items: any;
  constructor() { }

  ngOnInit() {
  }

}
