import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoGridComponent } from './logo-grid.component';

describe('LogoGridComponent', () => {
  let component: LogoGridComponent;
  let fixture: ComponentFixture<LogoGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
