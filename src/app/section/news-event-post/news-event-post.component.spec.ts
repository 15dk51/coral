import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsEventPostComponent } from './news-event-post.component';

describe('NewsEventPostComponent', () => {
  let component: NewsEventPostComponent;
  let fixture: ComponentFixture<NewsEventPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsEventPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsEventPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
