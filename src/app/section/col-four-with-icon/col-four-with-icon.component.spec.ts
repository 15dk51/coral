import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColFourWithIconComponent } from './col-four-with-icon.component';

describe('ColFourWithIconComponent', () => {
  let component: ColFourWithIconComponent;
  let fixture: ComponentFixture<ColFourWithIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColFourWithIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColFourWithIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
