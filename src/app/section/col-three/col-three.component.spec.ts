import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColThreeComponent } from './col-three.component';

describe('ColThreeComponent', () => {
  let component: ColThreeComponent;
  let fixture: ComponentFixture<ColThree>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
