import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';

@Component({
    selector: 'app-side-tabs',
    templateUrl: './side-tabs.component.html',
    styleUrls: ['./side-tabs.component.scss']
})
export class SideTabsComponent implements OnInit {
    public selectedIndex;
    constructor() {
    }

    // tab({currentTarget}) {
    //   let name, all;
    //   name = currentTarget.className;
    //   all = name.split(' ');
    //   $('.tab-text').removeClass('active');
    //   $('.tab-text.' + all[0]).addClass('active');
    // }
    setSelected(index) {
        this.selectedIndex = index;
    }
    ngOnInit() {
    }
}
