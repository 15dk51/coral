import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { HostListener } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NavigationComponent implements OnInit {
    public scroll: any;
    public navHeight: any;
    public winWidth: any;
  constructor() { }
    activemenu(){
      $('.navigation .hamburger').click(() => {
          $('.navigation .hamburger').toggleClass('activehamburger')
          $('.navigation .menu').slideToggle();
      } );
    }
    hideNav(){
        if(this.winWidth > 767){
            $('.navigation .menu').removeAttr('style');
        }
    }
  ngOnInit() {
      this.activemenu();
      this.scroll = $(document).scrollTop();
      this.navHeight = $('.navigation').height();
      this.winWidth = window.innerWidth;
      this.hideNav();
      window.onload = (e) => {
          this.winWidth = window.innerWidth;
          this.hideNav();
      };
      window.onresize = (e) => {
          this.winWidth = window.innerWidth;
          this.hideNav();
      };
  }
    @HostListener('window:scroll', [])
    onWindowScroll() {
        let scrolled;
        scrolled = $(document).scrollTop();
        if ((scrolled > this.scroll)) {
            if (scrolled > 20) {
                $('.navigation').addClass('colored-background');
            }
        } else if ((scrolled < this.scroll)) {
            if (scrolled <= 20) {
                $('.navigation').removeClass('colored-background');
            }
        }
        this.scroll = $(document).scrollTop();
    }

}
