import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomepageComponent } from './page/homepage/homepage.component';
import { NavigationComponent } from './part/navigation/navigation.component';
import { FooterComponent } from './part/footer/footer.component';
import { ColThreeComponent } from './section/col-three/col-three.component';
import { ColThreeBlockComponent } from './section/col-three-block/col-three-block.component';
import { ColTwoWithVideoComponent } from './section/col-two-with-video/col-two-with-video.component';
import { ContentWithIconComponent } from './section/content-with-icon/content-with-icon.component';
import { ContentWithImageComponent } from './section/content-with-image/content-with-image.component';
import { CtaComponent } from './section/cta/cta.component';
import { FooterCtaComponent } from './section/footer-cta/footer-cta.component';
import { HeroSliderComponent } from './section/hero-slider/hero-slider.component';
import { IntroComponent } from './section/intro/intro.component';
import { NewsEventPostComponent } from './section/news-event-post/news-event-post.component';
import { SecondLevelBannerComponent } from './section/second-level-banner/second-level-banner.component';
import { ServicesComponent } from './section/services/services.component';
import { SolutionsComponent } from './section/solutions/solutions.component';
import { TestimonialsComponent } from './section/testimonials/testimonials.component';
import { ThreeColumnsComponent } from './section/three-columns/three-columns.component';
import { TwoColumnsComponent } from './section/two-columns/two-columns.component';
import { WhatWeDoComponent } from './section/what-we-do/what-we-do.component';
import { AboutComponent } from './page/about/about.component';
import { BlogComponent } from './page/blog/blog.component';
import { BloglandingComponent } from './page/bloglanding/bloglanding.component';
import { ContactComponent } from './page/contact/contact.component';
import { ProductComponent } from './page/product/product.component';
import { NewsEventsComponent } from './page/news-events/news-events.component';
import { AppComponent } from './app.component';
import { ListallComponent } from './page/listall/listall.component';
import { ContactFormComponent } from './section/contact-form/contact-form.component';
import { ColWithIconComponent } from './section/col-with-icon/col-with-icon.component';
import { ColImageCtaComponent } from './section/col-image-cta/col-image-cta.component';
import { ColFourComponent } from './section/col-four/col-four.component';
import { TestimonialComponent } from './section/testimonial/testimonial.component';
import { LogoGridComponent } from './section/logo-grid/logo-grid.component';

import { TextSearchPipe } from './pipes/filter/text/text-search.pipe';
import { CommonService } from './services/common.service';
import { HeroslideService } from './services/effects/heroslide.service';
import { BodyWithImageComponent } from './section/body-with-image/body-with-image.component';
import { BlockQuoteComponent } from './section/block-quote/block-quote.component';
import { StatsComponent } from './section/stats/stats.component';
import { VideoBannerComponent } from './section/video-banner/video-banner.component';
import { ColTwoInfoComponent } from './section/col-two-info/col-two-info.component';
import { ColFourWithIconComponent } from './section/col-four-with-icon/col-four-with-icon.component';
import { SideTabsComponent } from './section/side-tabs/side-tabs.component';
import { SafeHtmlPipe } from './pipes/filter/html/safe-html.pipe';
import { CtaGrayComponent } from './section/cta-gray/cta-gray.component';
import { DocumentComponent } from './page/documentation/document/document.component';
import { uniquePipe } from './app.pipe';
import { FilterPipe } from './filter.pipe';


const appRoutes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'blogs', component: BloglandingComponent },
  { path: 'blog/:id', component: BlogComponent },
  { path: 'products', component: ProductComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'news-events', component: NewsEventsComponent },
  { path: 'all', component: ListallComponent},
  { path: 'documentation', component: DocumentComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavigationComponent,
    FooterComponent,
    ColThreeComponent,
    uniquePipe,
    FilterPipe,
    ColThreeBlockComponent,
    ColTwoWithVideoComponent,
    ContentWithIconComponent,
    ContentWithImageComponent,
    CtaComponent,
    FooterCtaComponent,
    HeroSliderComponent,
    IntroComponent,
    NewsEventPostComponent,
    SecondLevelBannerComponent,
    ServicesComponent,
    SolutionsComponent,
    TestimonialsComponent,
    ThreeColumnsComponent,
    TwoColumnsComponent,
    WhatWeDoComponent,
    AboutComponent,
    BlogComponent,
    BloglandingComponent,
    ContactComponent,
    ProductComponent,
    NewsEventsComponent,
    ListallComponent,
    ContactFormComponent,
    ColWithIconComponent,
    ColImageCtaComponent,
    ColFourComponent,
    TestimonialComponent,
    TextSearchPipe,
    LogoGridComponent,
    BodyWithImageComponent,
    BlockQuoteComponent,
    StatsComponent,
    VideoBannerComponent,
    ColTwoInfoComponent,
    ColFourWithIconComponent,
    SideTabsComponent,
    SafeHtmlPipe,
    CtaGrayComponent,
    DocumentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule   ,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [CommonService, HeroslideService],
  bootstrap: [AppComponent]
})
export class AppModule { }
