import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})

export class FilterPipe implements PipeTransform {
  transform(items: any[], term: string): any[] {
  
    if (!items) return [];
    else{
      var output = [], l = items.length, i;
        if(term ===''){
           output = items;
        }
      else{
        for(i=0;i< l;i++){
          if (items[i].taxonomy.indexOf(term) >= 0) {
            output.push(items[i]);
          }
        }
      }
      return output;
    }
  }
}