import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-listall',
  templateUrl: './listall.component.html',
  styleUrls: ['./listall.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListallComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
