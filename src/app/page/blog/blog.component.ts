import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  public item = {title: 'Lorem ipsum', author: 'Anonymous '};
  public id: number;
  public sub: any;
  constructor() { }

  ngOnInit() {
  }

}
