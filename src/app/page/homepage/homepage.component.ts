import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { CommonService } from '../../services/common.service';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageComponent implements OnInit {
  public hero_slider = '';
  public intro = '';
  public col_four_block = '';
  public testimonial = '';
  public logo_grid = '';
  public col_image_cta = '';
  public cta = '';
  public pagesections: any;
  constructor(private _commonService: CommonService) { }

  ngOnInit() {
    this._commonService.getContent('/assets/data/homepage.json').subscribe(res => {
      this.pagesections = Object.keys(res).map(key => [key]);
      // this.hero_slider = res.hero_slider;
     // this.intro = res.intro;
      this.col_image_cta = res.col_image_cta;
      this.cta = res.cta;
      this.col_four_block = res.col_four_block;
      this.testimonial = res.testimonial;
      this.logo_grid = res.logo_grid;
    }, err => {
      // do something if there is an error;
    }, () => {
      // do something when done;
    });
    this._commonService.getContent('http://dev-corald8.pantheonsite.io/api/homepage').subscribe(res => {
      console.log(res.intro)
      this.intro = res.intro;
      this.hero_slider = res.hero_slider;
    }, err => {
      console.log(err);
    }, () => {
      // do something when done;
    });
    this._commonService.getContent('https://dev-corald8.pantheonsite.io/api/paragraph/3').subscribe(res => {
      this.hero_slider = res.field_hero_slider;
    }, err => {
      // do something if there is an error;
    }, () => {
      // do something when done;
    });
  }

}
