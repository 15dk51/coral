import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
@Component({
  selector: 'app-bloglanding',
  templateUrl: './bloglanding.component.html',
  styleUrls: ['./bloglanding.component.scss']
})
export class BloglandingComponent implements OnInit {

  public item = {title: 'Blogs'};
  public items: any;
  public filterQuery =  '';
  public term = '';
  
  public searchTerm = function (val) {
    this.filterQuery = val;
  }
public abc = function (val) {
    this.term = val;
  }
  constructor(private _commonService: CommonService) {}

  ngOnInit() {
    this._commonService.getContent('/assets/data/posts.json').subscribe(res => {

      this.items = res;
    }, err => {
      // do something if there is an error;
    }, () => {
      // do something when done;
    });
    // this._commonService.getContent('http://local.corald8.com/api/block/1').subscribe(res => {
    //   console.log(res);
    // }, err => {
    //   // do something if there is an error;
    // }, () => {
    //   // do something when done;
    // });
  }
}
