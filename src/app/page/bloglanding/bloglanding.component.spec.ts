import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BloglandingComponent } from './bloglanding.component';

describe('BloglandingComponent', () => {
  let component: BloglandingComponent;
  let fixture: ComponentFixture<BloglandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloglandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloglandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
